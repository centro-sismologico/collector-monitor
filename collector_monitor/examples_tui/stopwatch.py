from textual.app import App, ComposeResult
from textual.containers import Container
from textual.widgets import Button, Header, Footer, Static
from time import monotonic
from textual.reactive import reactive

class TimeDisplay(Static):
    """A widget to display elapsed time."""
    start_time = reactive(monotonic)
    time = reactive(0.0)
    total = reactive(0.0)

    def on_mount(self)->None:
        self.update_timer = self.set_interval(1/60, self.update_time, pause=True)
        #self.set_interval(1/60, self.update_time)

    def update_time(self)->None:
        #self.time = monotonic() - self.start_time
        self.time = self.total + (monotonic() - self.start_time)

    def watch_time(self, time:float)-> None:
        minutes, seconds = divmod(time, 60)
        hours, minutes = divmod(minutes, 60)
        self.update(f"{hours:02.0f}:{minutes:02.0f}:{seconds:05.2f}")

    def start(self)->None:
        self.start_time = monotonic()
        self.update_timer.resume()

    def stop(self)->None:
        self.update_timer.pause()
        self.total += monotonic() - self.start_time
        self.time = self.total

    def reset(self):
        self.total = 0
        self.time = 0


class Stopwatch(Static):
    def compose(self) -> ComposeResult:
        """Create child widgets for the stopwatch"""
        yield Button("Iniciar", id="start", variant="success")
        yield Button("Detener", id="stop", variant="error")
        yield Button("Reset", id="reset")
        yield TimeDisplay("00:00:00.00")

    def on_button_pressed(self, event: Button.Pressed) -> None:
        button_id = event.button.id
        time_display = self.query_one(TimeDisplay)
        if button_id == "start":
            time_display.start()
            self.add_class("started")
        if button_id == "stop":
            time_display.stop()
            self.remove_class("started")
        if button_id == "reset":
            time_display.reset()


class CollectorMonitor(App):
    """A Textual app to manage stopwatches."""
    CSS_PATH = "css/monitor.css"
    BINDINGS = [
        ("d", "toggle_dark", "Toggle dark mode"),
        ("a", "add_stopwatch", "Add"),
        ("r", "remove_stopwatch", "Remove")
    ]

    def compose(self) -> ComposeResult:
        """Create child widgets for the app."""
        yield Header()
        yield Container(Stopwatch(), Stopwatch(), Stopwatch(), id="timers")
        yield Footer()

        print("Iniciando Collector Monitor")

    def action_add_stopwatch(self)->None:
        new_stopwatch = Stopwatch()
        self.query_one("#timers").mount(new_stopwatch)
        new_stopwatch.scroll_visible()

    def action_remove_stopwatch(self)->None:
        timers =  self.query("Stopwatch")
        print("TImers", timers)
        if timers:
            timers.last().remove()

    def action_toggle_dark(self) -> None:
        """An action to toggle dark mode."""
        self.dark = not self.dark


if __name__ == "__main__":
    app = CollectorMonitor()
    app.run()
