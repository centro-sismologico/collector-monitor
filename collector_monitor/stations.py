import httpx 
import json
from enum import IntEnum, auto
import time
import asyncio
from networktools.time import get_datetime_di

class Mode(IntEnum):
    GSOF = auto()
    SEED = auto()

SLEEP = 0.0001

async def load_stations(url, filter_tables=[]):
    mode = Mode.GSOF
    try:
        print("Leyendo", url)
        with httpx.Client() as client:
            u = client.get(url)
            while u.status_code != httpx.codes.OK:
                u = client.get(url)
                await asyncio.sleep(SLEEP)
            items = json.loads(u.content)
            for item in items:
                code = item.get("code")
                if mode == Mode.GSOF:
                    item["table_name"] = f"{code}_{mode.name}"
            items.sort(key=lambda item:-item["position"]["llh"]["lat"])
            return {item["code"]:item for item in items}
    except Exception as e:
        print(get_datetime_di(), f"Error al obtener lista de estaciones >{e}<")
        print(url)
        raise e
