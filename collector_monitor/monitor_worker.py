import asyncio

from dataclasses import dataclass
from pathlib import Path
from rich import print
from datetime import datetime, timedelta
from enum import IntEnum, auto
from tasktools.taskloop import TaskLoop
from .settings_manager import SettingsFile
#from .read_data import read_csv, Mode, load_stations
from .dbsteps import DBStep
from data_rdb import Rethink_DBS
from rethinkdb import RethinkDB
from networktools.time import get_datetime_di
from collections import Counter
from typing import Optional, List # python 3.9.2
import pytz
from rich.console import Console
from rich.table import Table
import ujson as json
from rich import box
import statistics as stats
import rethinkdb

local = pytz.utc
rdb = RethinkDB()

KEY = "DT_GEN"
FILTER_OPT = {'left_bound': 'open', 'index': KEY}
DAYS= 7.0
HOURS = 24.0
DATA_LIMIT = 100_000
STEP:float = 1

<<<<<<< HEAD
=======

>>>>>>> tui
def tramos(inicio, final, horas=3):
    lista = []
    tiempo = inicio

    while tiempo <= final:
        tiempo_last = tiempo + timedelta(hours=horas)
        lista.append((tiempo, tiempo_last))
        tiempo = tiempo_last

    return lista



@dataclass
class MonitorWorker:
    settings: SettingsFile
    step: float = STEP

    @property
    def days(self):
        """
        Fecha final, dias hacia atras
        """
        return self.settings.days or DAYS

    @property
    def hours(self):
        """
        Fecha final, dias hacia atras
        """
        return self.settings.hours or HOURS

    @property
    def destiny(self):
        return self.settings.destiny

    @property
    def origin(self):
        return self.settings.origin

    @property
    def sleep(self):
        return self.hours*60*60
    
    @property
    def days_seconds(self):
        return self.days*24*60*60

    def start_date(self):
        naive = self.settings.start_date() or datetime(2022,10,1)
        local_dt = local.localize(naive, is_dst=None)
        utc_dt = local_dt.astimezone(pytz.utc)
        # CONSTANTES
        # dias una hora atras
        return get_datetime_di(delta=3600)

    async def transfer_data(self, 
                            console,
                            di: datetime,
                            control_origin: DBStep, 
                            origin: Optional[Rethink_DBS],
                            tables: List[str],
                            *args,
                            **kwargs):
        
        print("Transfer data, esperar segundos:", self.sleep)
        if not di:
            control = True
            while control:
                try:
                    di = self.start_date()
                    control = False
                except:
                    print("Failed to read settings")
                    await asyncio.sleep(1)
            print("Set di to", di)

        if self.settings.origin_change:
            control_origin = DBStep.CREATE
            if origin:
                await origin.close()
                del origin
                origin = None

        raw_df = get_datetime_di(delta=0)
        lista_tramos = tramos(di, raw_df, horas=self.step)

        # create instances
        if control_origin == DBStep.CREATE:
            opts = self.origin.dict()
            kwargs["origin_db"] = self.origin
            origin = Rethink_DBS(**opts)
            control_origin = DBStep.CONNECT



        if control_origin == DBStep.CONNECT:
            try:
                await asyncio.wait_for(
                    origin.async_connect(), 
                    timeout=10)
                await origin.list_dbs()
                await origin.list_tables()
                tables = [n for n in origin.tables.get("collector")]
                control_origin = DBStep.COLLECT
            except asyncio.TimeoutError as te:
                control_origin = DBStep.CONNECT

        
        if control_origin == DBStep.COLLECT:
            dataset = {}
<<<<<<< HEAD
            for table_name in tables:
                control_read = True
                while control_read:
                    try:
                        total_cursor = 0
                        for (ndi,ndf) in lista_tramos:
                            task = asyncio.create_task(origin.get_data_filter(
                                    table_name,
                                    [ndi, ndf],
                                    FILTER_OPT,
                                    KEY, fields=("id",)))
                            cursor = await asyncio.shield(task)
                            if cursor:
                                total_cursor += len(cursor)
                        dataset[table_name] = total_cursor
                        control_read = False
                    except asyncio.exceptions.CancelledError as es:
                        print(f"Falla al consultar tabla {table_name}",
                              [di, raw_df])
                        self.step /=   2
                        lista_tramos = tramos(di, raw_df, horas=self.step)

                    except rethinkdb.errors.ReqlResourceLimitError as re:
                        print(f"Falla al consultar tabla {table_name}",
                              [di, raw_df],"Error limite", re)
                        self.step /=   2
                        lista_tramos = tramos(di, raw_df, horas=self.step)

=======
            try:
                for table_name in tables:
                    total_cursor = 0
                    for (ndi,ndf) in lista_tramos:
                        cursor = await origin.get_data_filter(
                                table_name,
                                [ndi, ndf],
                                FILTER_OPT,
                                KEY, fields=("id",))
                        if cursor:
                            total_cursor += len(cursor)
                    dataset[table_name] = total_cursor
                table = Table(
                    title=f"Reporte desde {di} a las {raw_df}")
                table.add_column("Tabla", justify="left",
                                 style="cyan")
                table.add_column("id", justify="left", style="white")
                table.add_column("Cantidad", justify="left", style="magenta")
                table.add_column("Cantidad [%]", justify="left", style="green")
                table.add_column("Station Info", justify="left", style="red")
                table.add_column("Total media desconectado", justify="left", style="red")
                table.add_column("Total std desconectado", justify="left", style="red")

                seconds = (raw_df-di).total_seconds()
                print(f"Total seconds {seconds}", di, raw_df)
                if seconds > 0:
                    counter = 1
                    for k,v in dataset.items():
                        if k not in kwargs:
                            kwargs[k] = [] 
                        desconexion = 1 - (v / seconds)
                        kwargs[k].append(desconexion)
                        media = 0
                        std = float('inf')
                        if (data:=kwargs.get(k),[0]):
                            media = stats.mean(data)
                            if len(data)>=2:
                                std = stats.stdev(data)
                        if desconexion > .5:
                            info = self.settings.station_info(k)

                            info = "gsof --code {} --host {} --port {}".format(
                                k.split("_")[0],
                                info.get("host"), 
                                info.get("port"))
                            table.add_row(str(counter),k,str(v), str(desconexion*100), info,
                                          str(media), str(std))
                            counter += 1
                console.print(table)
                di = raw_df
            except asyncio.exceptions.CancelledError as es:

                print(f"Falla al consultar tabla {table_name}",
                      [di, raw_df])
                self.step /=   2
>>>>>>> tui
                #si falla recalcular tramos a menor STEP
            table = Table(
                title=f"""Reporte estaciones esconectadas desde
                {di} a las {raw_df}""",
                show_header=True, box=box.ROUNDED, header_style="bold magenta",
                title_justify="center", expand=True)

            table.add_column("Tabla", justify="left", style="cyan")
            table.add_column("Cantidad", justify="left", style="magenta")
            table.add_column("Cantidad [%]", justify="left", style="green")
            table.add_column("Station Info", justify="left", style="red")
            table.add_column("Total media desconectado", justify="left", style="red")
            table.add_column("Total std desconectado", justify="left", style="red")

            seconds = (raw_df-di).total_seconds()

            if seconds > 0:
                for k,v in dataset.items():
                    if k not in kwargs:
                        kwargs[k] = [] 
                    desconexion = 1 - (v / seconds)
                    kwargs[k].append(desconexion)
                    media = 0
                    std = float('inf')
                    if (data:=kwargs.get(k),[0]):
                        media = stats.mean(data)

                #si falla recalcular tramos a menor STEP
            table = Table(
                title=f"""Reporte estaciones esconectadas desde
                {di} a las {raw_df}""",
                show_header=True, box=box.ROUNDED, header_style="bold magenta",
                title_justify="center", expand=True)

            table.add_column("Tabla", justify="left", style="cyan")
            table.add_column("Cantidad", justify="left", style="magenta")
            table.add_column("Cantidad [%]", justify="left", style="green")
            table.add_column("Station Info", justify="left", style="red")
            table.add_column("Total media desconectado", justify="left", style="red")
            table.add_column("Total std desconectado", justify="left", style="red")

            seconds = (raw_df-di).total_seconds()

            if seconds > 0:
                for k,v in dataset.items():
                    if k not in kwargs:
                        kwargs[k] = [] 
                    desconexion = 1 - (v / seconds)
                    kwargs[k].append(desconexion)
                    media = 0
                    std = float('inf')
                    if (data:=kwargs.get(k),[0]):
                        media = stats.mean(data)
                        if len(data) >= 2:
                            std = stats.stdev(data)

                    if desconexion > .5:
                        info = self.settings.station_info(k)

                        info = "gsof --code {} --host {} --port {}".format(
                            k.split("_")[0],
                            info.get("host"), 
                            info.get("port"))
                        table.add_row(k,str(v), str(desconexion*100), info,
                                      str(media), str(std))
            console.print(table)
            di = raw_df

        # obtener desde origin por cada tabla, los datos anteriores 'days' hacia
        # atras
        # guardar en destino cada dato en su tabla correspondiente
        await asyncio.sleep(self.sleep)
        return  [
            console, 
            di,
            control_origin, 
            origin, 
            tables,  
            *args
        ], kwargs


    def run(self):
        loop = asyncio.get_event_loop()
        control_origin = DBStep.CREATE
        tables = []
        console =  Console()
        di = None
        task = TaskLoop(
            self.transfer_data, 
            [console, di, control_origin,None,tables],{})
        task.create()
        if not loop.is_running():
            loop.run_forever()
