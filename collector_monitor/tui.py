from textual.app import App, ComposeResult
from textual.containers import Container
from textual.widgets import (Button, Header, Footer, Static, DataTable)
from time import monotonic
from textual.reactive import reactive
import io
from pathlib import Path
import csv

class TablaReporte(Static):
    CSV = Path(__file__).resolve().parent / "example.csv"

    def compose(self) -> ComposeResult:
        """Create child widgets for the stopwatch"""
        yield DataTable()

    def on_mount(self) -> None:
        table = self.query_one(DataTable)
        rows = csv.reader(io.StringIO(self.CSV.read_text()), delimiter=";")
        table.add_columns(*next(rows))
        table.add_rows(rows)

class CollectorMonitor(App):
    """A Textual app to manage stopwatches."""
    CSS_PATH = "css/monitor.css"
    BINDINGS = [
        ("d", "toggle_dark", "Toggle dark mode"),
    ]

    def compose(self) -> ComposeResult:
        """Create child widgets for the app."""
        yield Header()
        yield Container(TablaReporte(), id="tabla_reporte")
        yield Footer()

        print("Iniciando Collector Monitor")


    def action_toggle_dark(self) -> None:
        """An action to toggle dark mode."""
        self.dark = not self.dark


if __name__ == "__main__":
    app = CollectorMonitor()
    app.run()
